package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll(String userId, Comparator<E> comparator);

    List<E> findAll(String userId);

    E findById(String userId, String id);

    int size();

    int size(String userId);

    boolean existsById(String userId, String id);

    void clear(String userId);

    void removeById(String userId, String id);

    void add(E entity);

    void remove(E entity);

}
