package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    String getIdByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    boolean existsByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

}
