package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Task;

public interface ITaskService extends IService<Task> {

    Task add(String userId, String name, String description);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByName(String userId, String name, String nameNew, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task startTaskById(String userId, String id);

    Task startTaskByName(String userId, String name);

    Task startTaskByIndex(String userId, Integer index);

    Task finishTaskById(String userId, String id);

    Task finishTaskByName(String userId, String name);

    Task finishTaskByIndex(String userId, Integer index);

    Task setTaskStatusById(String userId, String id, Status status);

    Task setTaskStatusByName(String userId, String name, Status status);

    Task setTaskStatusByIndex(String userId, Integer index, Status status);

    String getIdByIndex(String userId, Integer index);

    boolean existsByName(String userId, String name);

    void removeOneByName(String userId, String name);

    void removeOneByIndex(String userId, Integer index);

    void removeOneById(String userId, String id);

}
