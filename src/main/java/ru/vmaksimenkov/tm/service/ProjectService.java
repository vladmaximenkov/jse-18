package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ComparatorNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, projectRepository.size())) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByName(final String userId, final String name, final String nameNew, final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (!checkIndex(index, projectRepository.size())) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, projectRepository.size(userId))) throw new IndexIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, projectRepository.size(userId))) throw new IndexIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project setProjectStatusById(final String userId, final String id, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project setProjectStatusByName(final String userId, final String name, final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project setProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (!checkIndex(index, projectRepository.size(userId))) throw new IndexIncorrectException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.existsByName(userId, name);
    }

}
