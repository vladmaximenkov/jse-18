package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.service.IAuthService;
import ru.vmaksimenkov.tm.api.service.IUserService;
import ru.vmaksimenkov.tm.exception.empty.EmptyLoginException;
import ru.vmaksimenkov.tm.exception.empty.EmptyPasswordException;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.exception.user.NotLoggedInException;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.util.HashUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User getUser() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new NotLoggedInException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void logout() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        final User user = userService.findByLogin(login);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}
