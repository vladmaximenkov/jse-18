package ru.vmaksimenkov.tm.exception.system;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Unknown command ```" + command + "``. Type help to see all available commands...");
    }

}
