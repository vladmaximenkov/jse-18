package ru.vmaksimenkov.tm.exception.entity;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class SortNotFoundException extends AbstractException {

    public SortNotFoundException() {
        super("Error! Sort not found...");
    }

}
