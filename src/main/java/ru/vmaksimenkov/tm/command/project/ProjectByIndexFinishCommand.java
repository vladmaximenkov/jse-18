package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIndexFinishCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().finishProjectByIndex(userId, TerminalUtil.nextNumber());
    }

}
