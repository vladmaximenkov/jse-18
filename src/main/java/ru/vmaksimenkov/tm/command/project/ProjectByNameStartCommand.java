package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByNameStartCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String description() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        serviceLocator.getProjectService().startProjectByName(userId, TerminalUtil.nextLine());
    }

}
