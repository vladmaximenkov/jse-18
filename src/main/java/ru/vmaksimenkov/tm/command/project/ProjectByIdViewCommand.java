package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIdViewCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Override
    public String description() {
        return "View project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        showProject(serviceLocator.getProjectService().findById(userId, TerminalUtil.nextLine()));
    }

}
