package ru.vmaksimenkov.tm.command.system;

import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Vlad Maximenkov");
        System.out.println("vmaksimenkov@tsconsulting.com");
    }
}
